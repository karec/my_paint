$(function () {
	var canvas = document.getElementById('draw');

	if (!canvas.getContext) {
		return false;
	}

	//attributions des variables
	var ctx   			= canvas.getContext('2d');
	var count 			= 0;
	var draw            = false;
	var position        = [];
	ctx.strokeStyle     = "black";
	var func            = 'pencil';
	ctx.lineWidth	    = 1;
	var fill			= false;
	var tmpColor		= "white";

	ctx.lineJoin = 'round';
	ctx.lineCap  = 'round';

	//gestion des events
	$('#draw').click(function (e) {
		position[count] = [e.pageX - this.offsetLeft, e.pageY - this.offsetTop];
		count = count + 1;
		if (count == 2) {
			if (func == 'line')
				line(position);
			else if (func == 'rect')
				rect(position);
			else if (func == 'circle')
				circle(position);
			count = 0;
			position = [];
		}
	});
 
	$('#draw').mousedown(function (e) {
		if (func != "pencil" && func != "eraser")
			return false;
		draw = true;
		if (func == "eraser") {
			tmpColor = ctx.strokeStyle;
			ctx.strokeStyle = "white";
		}
		ctx.beginPath();
		ctx.moveTo(e.pageX - this.offsetLeft, e.pageY - this.offsetTop);
	});

	$('#draw').mouseout(function (e) {
		if (draw)
			draw = false;
	});

	$('#draw').mousemove(function (e) {
		if (draw === true) {
			ctx.lineTo(e.pageX - this.offsetLeft, e.pageY - this.offsetTop);
			ctx.stroke();
		}
	});

	$('#draw').mouseup(function () {
		if (draw === true)
			draw = false;
		if (func == "eraser") {
			ctx.strokeStyle = tmpColor;
		}
	});

	$('.color').click(function () {
		$('.color').removeClass('active');
		$(this).addClass('active');
		ctx.strokeStyle = $(this).attr('id');
	});

	$('.tools').click(function () {
		$('.tools').removeClass('active');
		$(this).addClass('active');
		func = $(this).attr('id');
		position = [];
		count = 0;
	});

	$('#size').change(function (e) {
		if ($(this).val() > 0)
			ctx.lineWidth = $(this).val();
	});

	$('#size').click(function (e) {
		if ($(this).val() > 0)
			ctx.lineWidth = $(this).val();
	});

	$('#size').keypress(function (e) {
		if ($(this).val() > 0)
			ctx.lineWidth = $(this).val();
	});

	$('#clear').click(function () {
		clearCtx();
	});

	$('#fill').click(function () {
		if (fill)
			fill = false;
		else
			fill = true;
	});

	//gestion des couleurs
	function modColor()
	{
		var red   = $('#red').val();
		var blue  = $('#blue').val();
		var green = $('#green').val();

		if (red > 255)
			$('#red').val(255);
		if (blue > 255)
			$('#blue').val(255);
		if (green > 255)
			$('#green').val(255);
		$('.actualColor').css("background-color", "rgb("+red+", "+green+", "+blue+")");
		ctx.strokeStyle = "rgb("+red+", "+green+", "+blue+")";
		ctx.fillStyle   = "rgb("+red+", "+green+", "+blue+")";
	}

	$('.formColor').change(function () {
		modColor();
	});

	//fonctions de dessin
	function rect(position)
	{
		ctx.beginPath();
		ctx.moveTo(position[0][0], position[0][1]);
		if (fill)
			ctx.fillRect(position[0][0], position[0][1], position[1][0] - position[0][0], position[1][1] - position[0][1]);
		else
			ctx.strokeRect(position[0][0], position[0][1], position[1][0] - position[0][0], position[1][1] - position[0][1]);

	}

	function line(position)
	{
		ctx.beginPath();
		ctx.moveTo(position[0][0], position[0][1]);
		ctx.lineTo(position[1][0], position[1][1]);
		ctx.stroke();
	}

	function circle(position)
	{
		var xS    = position[0][0];
		var xE    = position[1][0];
		var yS    = position[0][1];
		var yE    = position[1][1];
		var rayon = Math.sqrt(Math.pow(xS - xE, 2) + (Math.pow(yS - yE, 2)));
		ctx.beginPath();
		ctx.arc(position[0][0], position[0][1], rayon, 0, 270, false);
		if (fill)
			ctx.fill();
		else
			ctx.stroke();
	}

	function clearCtx()
	{
		ctx.beginPath();
		ctx.clearRect(0, 0, 900, 700);
	}
});
